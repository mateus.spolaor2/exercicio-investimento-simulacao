package br.com.investimentos.services;

import br.com.investimentos.DTOs.InvestimentosDTO;
import br.com.investimentos.models.Investimento;
import br.com.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class InvestimentoServiceTeste {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    Investimento investimento;

    List<Investimento> investimentos;

    @BeforeEach
    private void setup(){

        this.investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("CDB");
        investimento.setRendimentoAoMes(2.9);

        this.investimentos = Arrays.asList(investimento);
    }

    @Test
    public void testaCadastroDeInvestimento(){
        InvestimentosDTO investimentoDTO = new InvestimentosDTO();
        investimentoDTO.setNome("Fundo");
        investimentoDTO.setRendimentoAoMes(4.19);

        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).then(objeto -> objeto.getArgument(0));

        Investimento testeDeInvestimento = investimentoService.cadastraNovoInvestimento(investimentoDTO);

        Assertions.assertEquals(investimentoDTO.getNome(), testeDeInvestimento.getNome());
        Assertions.assertEquals(investimentoDTO.getRendimentoAoMes(), testeDeInvestimento.getRendimentoAoMes());

    }

}
