package br.com.investimentos.services;

import br.com.investimentos.DTOs.InvestimentosDTO;
import br.com.investimentos.DTOs.SimulacaoRequestDTO;
import br.com.investimentos.DTOs.SimulacaoResponseDTO;
import br.com.investimentos.models.Investimento;

import br.com.investimentos.models.Simulacao;
import br.com.investimentos.repositories.InvestimentoRepository;
import br.com.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    InvestimentoRepository investimentoRepository;

    @Autowired
    SimulacaoService simulacaoService;

    public List<Investimento> consultaInvestimentos() {
        List<Investimento> listaDeInvestimentos = new ArrayList<>();
        for(Investimento investimento : investimentoRepository.findAll()){

            Investimento investimentoConsultado =
                    new Investimento(investimento.getId(), investimento.getNome(), investimento.getRendimentoAoMes());

            listaDeInvestimentos.add(investimentoConsultado);
        }
        return listaDeInvestimentos;
    }

    public Investimento cadastraNovoInvestimento(InvestimentosDTO investimentoDTO) {

        Investimento investimento = new Investimento();
        investimento.setNome(investimentoDTO.getNome());
        investimento.setRendimentoAoMes(investimentoDTO.getRendimentoAoMes());

        Investimento response = investimentoRepository.save(investimento);

        return response;

    }

    public SimulacaoResponseDTO projetaSimulacao(int id, SimulacaoRequestDTO simulacaoRequestDTO) {
        Investimento investimento = buscarInvestimentoPeloId(id);
        Simulacao simulacaoProjetada = simulacaoService.projetaSimulacao(investimento, simulacaoRequestDTO);

        SimulacaoResponseDTO response = new SimulacaoResponseDTO();
        response.converteSimulacaoParaDTO(simulacaoProjetada);

        return response;
    }

    public Investimento buscarInvestimentoPeloId(int id){
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);

        if(investimentoOptional.isPresent()){
            Investimento investimento = investimentoOptional.get();
            return investimento;
        }else {
            throw new RuntimeException("O investimento não foi encontrado");
        }
    }
}
