package br.com.investimentos.services;

import br.com.investimentos.DTOs.SimulacaoDTO;
import br.com.investimentos.DTOs.SimulacaoRequestDTO;
import br.com.investimentos.models.Investimento;
import br.com.investimentos.models.Simulacao;
import br.com.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;


    public Simulacao projetaSimulacao(Investimento investimento, SimulacaoRequestDTO simulacaoRequestDTO) {
        Simulacao simulacao = converteParaSimulacaoModel(investimento, simulacaoRequestDTO);
        Simulacao simulacaoProjetada = simulacaoRepository.save(simulacao);

        return simulacaoProjetada;

    }

    private Simulacao converteParaSimulacaoModel(Investimento investimento, SimulacaoRequestDTO simulacaoRequestDTO) {
        Simulacao simulacao = new Simulacao();
        simulacao.setEmail(simulacaoRequestDTO.getEmail());
        simulacao.setNomeCliente(simulacaoRequestDTO.getNomeInteressado());
        simulacao.setQuantidadeMeses(simulacaoRequestDTO.getQuantidadeMeses());
        simulacao.setValorAplicado(simulacaoRequestDTO.getValorAplicado());
        simulacao.setInvestimento(investimento);

        return simulacao;
    }

    public List<SimulacaoDTO> consultaSimulacoes() {
        List<SimulacaoDTO> listaDeSimulacoes = new ArrayList<>();
        for(Simulacao simulacao : simulacaoRepository.findAll()){

            SimulacaoDTO simulacaoConsultada = new SimulacaoDTO();
            simulacaoConsultada.setValorAplicado(simulacao.getValorAplicado());
            simulacaoConsultada.setInvestimentoID(simulacao.getInvestimento().getId());
            simulacaoConsultada.setQuantidadeMeses(simulacao.getQuantidadeMeses());
            simulacaoConsultada.setNomeInteressado(simulacao.getNomeCliente());
            simulacaoConsultada.setEmail(simulacao.getEmail());

            listaDeSimulacoes.add(simulacaoConsultada);
        }
        return listaDeSimulacoes;
    }
}
