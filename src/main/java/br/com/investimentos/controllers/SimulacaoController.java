package br.com.investimentos.controllers;

import br.com.investimentos.DTOs.SimulacaoDTO;
import br.com.investimentos.DTOs.SimulacaoResponseDTO;
import br.com.investimentos.models.Investimento;
import br.com.investimentos.models.Simulacao;
import br.com.investimentos.services.InvestimentoService;
import br.com.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;


    @GetMapping
    public List<SimulacaoDTO> consultaInvestimentosDTOS(){
        List<SimulacaoDTO> listaDeSimulacoes = simulacaoService.consultaSimulacoes();
        return listaDeSimulacoes;
    }



}
