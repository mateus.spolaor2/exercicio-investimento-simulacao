package br.com.investimentos.controllers;

import br.com.investimentos.DTOs.InvestimentosDTO;
import br.com.investimentos.DTOs.SimulacaoRequestDTO;
import br.com.investimentos.DTOs.SimulacaoResponseDTO;
import br.com.investimentos.models.Investimento;
import br.com.investimentos.models.Simulacao;
import br.com.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;

    @GetMapping
    public List<Investimento> consultaInvestimentosDTOS(){
        List<Investimento> listaDeInvestimentos = investimentoService.consultaInvestimentos();
        return listaDeInvestimentos;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento cadastraNovoInvestimento(@RequestBody @Valid InvestimentosDTO novoInvestimento){
        Investimento investimentoCadastrado = investimentoService.cadastraNovoInvestimento(novoInvestimento);
        return investimentoCadastrado;
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoResponseDTO projetaSimulacao(
            @PathVariable(name = "id") int id,
            @RequestBody @Valid SimulacaoRequestDTO simulacaoRequestDTO){
        try{
            SimulacaoResponseDTO response = investimentoService.projetaSimulacao(id, simulacaoRequestDTO);
            return response;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
