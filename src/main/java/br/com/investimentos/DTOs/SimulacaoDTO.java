package br.com.investimentos.DTOs;

public class SimulacaoDTO {

    private int investimentoID;
    private String nomeInteressado;
    private String email;
    private Double valorAplicado;
    private int quantidadeMeses;

    public SimulacaoDTO() {}

    public SimulacaoDTO(int investimentoID, String nomeInteressado, String email, Double valorAplicado, int quantidadeMeses) {
        this.investimentoID = investimentoID;
        this.nomeInteressado = nomeInteressado;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
    }

    public int getInvestimentoID() {
        return investimentoID;
    }

    public void setInvestimentoID(int investimentoID) {
        this.investimentoID = investimentoID;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }
}
