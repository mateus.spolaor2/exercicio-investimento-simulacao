package br.com.investimentos.DTOs;

import javax.validation.constraints.Digits;

public class InvestimentosDTO {
    private String nome;
    @Digits(integer = 2, fraction = 2, message = "Rendimento fora do padrão")
    private double rendimentoAoMes;

    public InvestimentosDTO() {
    }

    public InvestimentosDTO(String nome, double rendimentoAoMes) {
        this.nome = nome;
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
