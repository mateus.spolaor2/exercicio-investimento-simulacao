package br.com.investimentos.models;

import javax.persistence.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nomeCliente;
    private String email;
    private Double valorAplicado;
    private int quantidadeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimento;

    public Simulacao() {
    }

    public Simulacao(int id, String nomeCliente, String email, Double valorAplicado, int quantidadeMeses, Investimento investimento) {
        this.id = id;
        this.nomeCliente = nomeCliente;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
        this.investimento = investimento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
